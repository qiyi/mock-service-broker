#!/usr/bin/env ruby

$:.unshift(File.expand_path("../../lib", __FILE__))

ENV["BUNDLE_GEMFILE"] ||= File.expand_path("../../Gemfile", __FILE__)

require "rubygems"
require "bundler/setup"
require "mock/service/broker"

Mock::Service::Broker::App.run!

