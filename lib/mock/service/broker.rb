require "sinatra/base"
require "mock/service/broker/version"

module Mock
  module Service
    module Broker
      class App < Sinatra::Application
        set :bind, '0.0.0.0'

        get "/v2/catalog" do
          File.open("config/catalog.json", "r") do |f|
            f.read
          end
        end

        put "/v2/service_instances/:id" do |id|
          '{"dashboard_url": "http://isouth.org/mock-service-dashboard"}'
        end

        put "/v2/service_instances/:instance_id/service_bindings/:id" do 
          '{"credentials": {"secret": "learning ruby"}}'
        end

        delete "/v2/service_instances/:instance_id/service_bindings/:id" do
          '{}'
        end

        delete "/v2/service_instances/:id" do
          '{}'
        end

      end
    end
  end
end
