# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'mock/service/broker/version'

Gem::Specification.new do |spec|
  spec.name          = "mock-service-broker"
  spec.version       = Mock::Service::Broker::VERSION
  spec.authors       = ["l00195613"]
  spec.email         = ["bphanzhu@gmail.com"]
  spec.summary       = %q{Mock service broker.}
  spec.description   = %q{Mock service broker.}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.7"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "sinatra"
  spec.add_development_dependency "thin"
end
